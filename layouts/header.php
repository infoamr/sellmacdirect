<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Sell Mac Direct</title>
    <link rel="icon" href="images/favicon.ico" type="image/gif" sizes="16x16">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">


      <script src="js/jquery.min.js"></script>


    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
  </head>
  <body style="background-image: linear-gradient(to left, rgb(77, 119, 78), rgb(99, 133, 76), rgb(125, 146, 72), rgb(156, 158, 68), rgb(190, 168, 67), rgb(199, 174, 73), rgb(207, 181, 78), rgb(216, 187, 84), rgb(198, 192, 96), rgb(182, 196, 110), rgb(168, 199, 126), rgb(157, 200, 141));">
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.php">
			<img class="normal"src="images/sellmacv5.png" width="127" alt="Sell_Mac_Direct_Logo">
			<img class="light-sticky" src="images/sellmacv5foot.png" width="127" alt="Sell_Mac_Direct_Logo">
		  </a>
	      <!--<img class="light" img src="/images/sellmacv5foot.png" width="127" alt="Sell_Mac_Direct_Logo"></a>-->
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item active"><a href="index.php" class="nav-link">Home</a></li>
	          <li class="nav-item"><a href="macbook.php" class="nav-link">MacBook</a></li>
	          <li class="nav-item"><a href="macbookpro.php" class="nav-link">MacBook Pro</a></li>
            <li class="nav-item"><a href="macbookair.php" class="nav-link">MacBook Air</a></li>
	          <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->