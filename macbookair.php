<?php include 'layouts/header.php';?>

<section class="ftco-section ftco-no-pb" style="background-image: linear-gradient(to left, #4d774e, #63854c, #7d9248, #9c9e44, #bea843, #c7ae49, #cfb54e, #d8bb54, #c6c060, #b6c46e, #a8c77e, #9dc88d);" data-stellar-background-ratio="0.5">
  <div class="condition-top-section">
    <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <h1 class="mb-4" style="none">It’s easy to sell your Macbook for cash!</h1>
            <p style="font-size: 18px;">In order to offer an accurate price we need to know the age and specification of your macbook</p>
          </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
               <div id="progress-bar" class="row condition-progress-bar">
                  <div class="col-3 cp-box cp-box-process cp-box-active js-active first">
                     <span style="font-size: 18px;">Select Type</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box">
                     <span style="font-size: 18px;">Select Generation</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box">
                     <span style="font-size: 18px;">Condition &amp; Functionality</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box last">
                     <span style="font-size: 18px;">Confirm offer</span>
                     <div class="tick"></div>
                  </div>
               </div>
            </div>
        </div>
    </div>
  </div>
<!-- ---------------------Type Section Start---------------------- --> 
<section class="flex-grow-1 d-flex flex-column">
    <div class="buttons condition-box">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4 class="mb-4">Select Your Mac Air Type</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px"  data-toggle="modal" data-target="#js-help-modal" data-href="">
                    <span>How do I find this</span> 
                    <picture>
                        <img src="images/question.png" alt="Help"> 
                    </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from macbook_ver where mac_id='air' ";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo "<div class='container'>";
                echo " <div class='row justify-content-center'>";
                while($row = $result->fetch_assoc()) {
                    echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 box2 mb-4' >";
                    if($row['id']=='10') {
                        echo " <a  class='show box air_type_2' target='2' data-t_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/macbook.png'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    elseif($row['id']=='11'){
                        echo " <a  class='show box air_type_3' target='3' data-t_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/macbook.png'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    elseif($row['id']=='13'){
                        echo " <a  class='show box air_type_4' target='4' data-t_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/macbook.png'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    else{
                        echo " <a  class='show box air_type_1' target='1' data-t_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/macbook.png'>";
                        echo " </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
                echo"</div>";
            }
            ?>
        </div>
    </div>
<!-- --------------------Type Section End---------------------- -->
    
<!-- /////////////////////// Macbook Air (2009 - Late 2010) ///////////////////////// -->
<!-- Processor Section -->
<div class="container">
        <div id="div1" class="targetDiv">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Processor</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
                       <span>How do I find this</span>
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="row justify-content-center">
             
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from macbook_prs WHERE id='12' ";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row

                while($row = $result->fetch_assoc()) {
                    
                    echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                    echo "<a  class='show2 box air_prs_1_1' target='11' data-p_id='{$row['id']}'>";
                    echo "<div class='box-image'>";
                    echo "<img src='images/intelcore2.png'  alt='Macbook Intel Core i2'>";
                    echo" </div>";
                    echo "<p style='font-size:18px;'>".trim($row['name'])."</p>";
                    echo "</p>";
                    echo "</form>";
                    echo "</a>";
                    echo "</div>";
                }
            }
            echo "</div>";
            ?>            
            </div>
        </div>
    </div>
    <!-- End Processor Section -->

    <!-- Start Screen Section --> 
    <div class="container">
        <div id="div11" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                $servername = "localhost";
                $username = "sellmacuser";
                $password = "5173kor8@X4@";
                $dbname = "sellmacbook";

                $conn = new mysqli($servername, $username, $password, $dbname);

                // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==1) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_11_1' target='111' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_11_2' target='112' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-13.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>
    <!-- End Screen Section -->
    <!-- Start Model Section -->
    <div class="container">
        <div id="div111" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==13){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair1Form'>";
                        echo " <div class='box-image' onclick='myair1Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_1'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_1_core2'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_11_1_11'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    <div class="container">
        <div id="div112" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==26){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair2Form'>";
                        echo " <div class='box-image' onclick='myair2Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_1'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_1_core2'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_11_1_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==11){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair3Form'>";
                        echo " <div class='box-image' onclick='myair3Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_1'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_1_core2'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_11_1_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==12){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair4Form'>";
                        echo " <div class='box-image' onclick='myair4Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_1'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_1_core2'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_11_1_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    <!-- End Model Section -->
<!-- /////////////////////// End Macbook Air (2009 - Late 2010) ///////////////////////// -->





<!-- /////////////////////// Start Macbook Air (2011 - 2012) ///////////////////////// -->
<!-- Processor Section -->
<div class="container">
        <div id="div2" class="targetDiv">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Processor</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
                       <span>How do I find this</span>
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="row justify-content-center">
             
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from macbook_prs";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row

                while($row = $result->fetch_assoc()) {
                    if($row['id']==14){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a class='show2 box air_prs_i5' target='21' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorei5.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==13){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a  class='show2 box air_prs_i7 ' target='22' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorei7.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
            }
            echo "</div>";
            ?>            
            </div>
        </div>
    </div>
    <!-- End Processor Section -->

    <!-- Start Screen Section --> 
    <div class="container">
        <div id="div21" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                $servername = "localhost";
                $username = "sellmacuser";
                $password = "5173kor8@X4@";
                $dbname = "sellmacbook";

                $conn = new mysqli($servername, $username, $password, $dbname);

                // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==1) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_11' target='211' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_13' target='212' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-13.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="div22" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                $servername = "localhost";
                $username = "sellmacuser";
                $password = "5173kor8@X4@";
                $dbname = "sellmacbook";

                $conn = new mysqli($servername, $username, $password, $dbname);

                // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==1) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_11' target='221' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_13' target='222' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-13.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>
    <!-- End Screen Section -->
    <!-- Start Model Section -->
    <div class="container">
        <div id="div211" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==13){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair5Form'>";
                        echo " <div class='box-image' onclick='myair5Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_2'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i5'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_11'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==14){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair6Form'>";
                        echo " <div class='box-image' onclick='myair6Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_2'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i5'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_11'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    <div class="container">
        <div id="div212" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==12){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair7Form'>";
                        echo " <div class='box-image' onclick='myair7Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_2'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i5'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==15){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair8Form'>";
                        echo " <div class='box-image' onclick='myair8Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_2'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i5'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    <div class="container">
        <div id="div221" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==13){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair9Form'>";
                        echo " <div class='box-image' onclick='myair9Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_2'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i7'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_11'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==14){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair10Form'>";
                        echo " <div class='box-image' onclick='myair10Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_2'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i7'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_11'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    <div class="container">
        <div id="div222" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==12){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair11Form'>";
                        echo " <div class='box-image' onclick='myair11Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_2'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i7'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==15){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair12Form'>";
                        echo " <div class='box-image' onclick='myair12Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_2'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i7'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    <!-- End Model Section -->
<!-- /////////////////////// End Macbook Air (2011 - 2012) ///////////////////////// -->




<!-- /////////////////////// Start Macbook Air (2013 - 2020) ///////////////////////// -->
    <!-- Processor Section -->
<div class="container">
        <div id="div3" class="targetDiv">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Processor</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
                       <span>How do I find this</span>
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="row justify-content-center">
             
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from macbook_prs";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row

                while($row = $result->fetch_assoc()) {
                    if($row['id']==14){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a class='show2 box air_prs_i5 ' target='31' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorei5.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==13){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a  class='show2 box air_prs_i7 ' target='32' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorei7.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
            }
            echo "</div>";
            ?>            
            </div>
        </div>
    </div>
    <!-- End Processor Section -->

    <!-- Start Screen Section --> 
    <div class="container">
        <div id="div31" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                $servername = "localhost";
                $username = "sellmacuser";
                $password = "5173kor8@X4@";
                $dbname = "sellmacbook";

                $conn = new mysqli($servername, $username, $password, $dbname);

                // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==1) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_11' target='311' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_13' target='312' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-13.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="div32" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                $servername = "localhost";
                $username = "sellmacuser";
                $password = "5173kor8@X4@";
                $dbname = "sellmacbook";

                $conn = new mysqli($servername, $username, $password, $dbname);

                // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==1) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_11' target='321' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_13' target='322' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-13.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <!-- End Screen Section -->

    <!-- Start Model Section -->
    <div class="container">
        <div id="div311" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==14){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair13Form'>";
                        echo " <div class='box-image' onclick='myair13Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_3'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i5'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_11'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    <div class="container">
        <div id="div312" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==15){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair14Form'>";
                        echo " <div class='box-image' onclick='myair14Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_3'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i5'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==17){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair15Form'>";
                        echo " <div class='box-image' onclick='myair15Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_3'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i5'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    <div class="container">
        <div id="div321" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==14){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair16Form'>";
                        echo " <div class='box-image' onclick='myair16Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_3'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i7'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_11'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    <div class="container">
        <div id="div322" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==15){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair17Form'>";
                        echo " <div class='box-image' onclick='myair17Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_3'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i7'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    
    <!-- End Model Section -->
<!-- /////////////////////// End Macbook Air (2013 - 2020) ///////////////////////// -->







<!-- /////////////////////// Start Macbook Air Silicon (2020 - Current) ///////////////////////// -->
<!-- Processor Section -->
<div class="container">
        <div id="div4" class="targetDiv">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Processor</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
                       <span>How do I find this</span>
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="row justify-content-center">
             
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from macbook_prs";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row

                while($row = $result->fetch_assoc()) {
                    if($row['id']==15){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a class='show2 box air_prs_i3 ' target='41' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorei3.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==14){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a  class='show2 box air_prs_i5 ' target='42' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorei5.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==13){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a  class='show2 box air_prs_i7 ' target='43' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorei7.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==17){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a  class='show2 box air_prs_m1 ' target='44' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/macm1.jpg' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
            }
            echo "</div>";
            ?>            
            </div>
        </div>
    </div>
    <!-- End Processor Section -->

    <!-- Start Screen Section --> 
    <div class="container">
        <div id="div41" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                $servername = "localhost";
                $username = "sellmacuser";
                $password = "5173kor8@X4@";
                $dbname = "sellmacbook";

                $conn = new mysqli($servername, $username, $password, $dbname);

                // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_13' target='411' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-13.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="div42" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                $servername = "localhost";
                $username = "sellmacuser";
                $password = "5173kor8@X4@";
                $dbname = "sellmacbook";

                $conn = new mysqli($servername, $username, $password, $dbname);

                // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_13' target='421' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-13.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="div43" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                $servername = "localhost";
                $username = "sellmacuser";
                $password = "5173kor8@X4@";
                $dbname = "sellmacbook";

                $conn = new mysqli($servername, $username, $password, $dbname);

                // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_13' target='431' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-13.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="div44" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                $servername = "localhost";
                $username = "sellmacuser";
                $password = "5173kor8@X4@";
                $dbname = "sellmacbook";

                $conn = new mysqli($servername, $username, $password, $dbname);

                // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_air_13' target='441' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-13.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <!-- End Screen Section -->

    <!-- Start Model Section -->
    <div class="container">
        <div id="div411" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==16){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair18Form'>";
                        echo " <div class='box-image' onclick='myair18Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_4'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i3'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    <div class="container">
        <div id="div421" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==16){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair19Form'>";
                        echo " <div class='box-image' onclick='myair19Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_4'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i5'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    <div class="container">
        <div id="div431" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==16){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair20Form'>";
                        echo " <div class='box-image' onclick='myair20Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_4'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_i7'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    <div class="container">
        <div id="div441" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Air Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "sellmacuser";
            $password = "5173kor8@X4@";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==27){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='myair21Form'>";
                        echo " <div class='box-image' onclick='myair21Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_air_4'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_air_prs_m1'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_air_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
    
    <!-- End Model Section -->
<!-- /////////////////////// End Macbook Air Silicon (2020 - Current) ///////////////////////// -->

<!-- Find Serial Number Pop Up starts -->
    <div class="modal fade" id="js-help-modal" role="dialog" tabindex="-1" aria-hidd="true">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content" style="margin-top:120px;">
                <div class="modal-body position-relative" style="overflow-y: auto;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                    <div class="container-fluid">
                        <div class="row mx-0">
                            <div class="mb-5">
                                <h1 class="mb-4">How do I find Serial Number - MACBOOK</h1>
                                <p>You can find your devices serial number by one of the following methods.</p>
                                <h4 class="mt-4">If your device powers on:</h4>
                                <p>1. Click the Apple icon at the top left of the screen.</p>
                                <img src="images/how_to_find.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I Find Screen Size"> 
                                <p>2. Click “About This Mac”</p>
                                <img src="images/convertto.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I Find Screen Size"> 
                                <p>3. Your serial number will be stated within the details window. You can copy the serial by highlighting the text and pressing <kbd><kbd>Cmd</kbd> + <kbd>C</kbd></kbd>.</p>
                                <img src="images/xserial.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I find The Serial Number"> 
                                <h4 class="mt-4">If your device does not power on:</h4>
                                <p>1. On the underside of your mac there will be some text below the device type, check for the text “Serial”, the following numbers will be the device serial number.</p>
                                <img src="images/macbookserial.png" class="img-fluid rounded shadow-sm d-block mx-auto" alt="How Do I find The Serial Number"> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!-- Find Serial Number Pop Up starts -->
</section> 

<?php include 'layouts/footer.php';?>
<script>
//On click of the 'next' anchor
$('.accordion--form__next-btn').on('click touchstart', function() {

  var parentWrapper = $(this).parent().parent();
  var nextWrapper = $(this).parent().parent().next('.accordion--form__fieldset');
  var sectionFields = $(this).siblings().find('.required');


  //Validate the .required fields in this section
  var empty = $(this).siblings().find('.required').filter(function() {

    return this.value === "";

  });

  if (empty.length) {

    $('.accordion--form__invalid').show();

  } else {

    $('.accordion--form__invalid').hide();

    //If valid
    //On the next fieldset -> accordion wrapper, toggle the active class
    nextWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

    //Close the others
    parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

    //Add a class to the parent legend
    nextWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

    //Remove the active class from the other legends
    parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');

  }

  return false;
});

//On click of the 'prev' anchor
$('.accordion--form__prev-btn').on('click touchstart', function() {

  parentWrapper = $(this).parent().parent();
  prevWrapper = $(this).parent().parent().prev('.accordion--form__fieldset');

  //On the prev fieldset -> accordion wrapper, toggle the active class
  prevWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

  //Close the others
  parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

  //Add a class to the parent legend
  prevWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

  //Remove the active class from the other legends
  parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');


  return false;
});


function sendContact() {
  var valid;  
  valid = validateContact();
  if(valid) {
    jQuery.ajax({
    url: "contact_mail.php",
    data:'userName='+$("#userName").val()+'&userEmail='+$("#userEmail").val()+'&subject='+$("#userName","#subject","#userEmail").val()+'&message='+$("#content").val()+'&content='+$('#html-content-holder').html(),
    type: "POST",
    success:function(data){
    $("#mail-status").html(data);
    },
    error:function (){}
    });
  }
}

function myairFunction() {
  document.getElementById("myairForm").submit();
}
function myair1Function() {
  document.getElementById("myair1Form").submit();
}
function myair2Function() {
  document.getElementById("myair2Form").submit();
}

function myair3Function() {
  document.getElementById("myair3Form").submit();
}

function myair4Function() {
  document.getElementById("myair4Form").submit();
}

function myair5Function() {
  document.getElementById("myair5Form").submit();
}
function myair6Function() {
  document.getElementById("myair6Form").submit();
}
function myair7Function() {
  document.getElementById("myair7Form").submit();
}
function myair7Function() {
  document.getElementById("myair7Form").submit();
}
function myair8Function() {
  document.getElementById("myair8Form").submit();
}
function myair9Function() {
  document.getElementById("myair9Form").submit();
}
function myair10Function() {
  document.getElementById("myair10Form").submit();
}
function myair11Function() {
  document.getElementById("myair11Form").submit();
}
function myair12Function() {
  document.getElementById("myair12Form").submit();
}
function myair13Function() {
  document.getElementById("myair13Form").submit();
}
function myair14Function() {
  document.getElementById("myair14Form").submit();
}
function myair15Function() {
  document.getElementById("myair15Form").submit();
}
function myair16Function() {
  document.getElementById("myair16Form").submit();
}
function myair17Function() {
  document.getElementById("myair17Form").submit();
}
function myair18Function() {
  document.getElementById("myair18Form").submit();
}
function myair19Function() {
  document.getElementById("myair19Form").submit();
}
function myair20Function() {
  document.getElementById("myair20Form").submit();
}
function myair21Function() {
  document.getElementById("myair21Form").submit();
}
function myairbFunction() {
  document.getElementById("myairbForm").submit();
}
function myairbbFunction() {
  document.getElementById("myairbbForm").submit();
}
$('.targetDiv').hide();
$('.show').click(function () {
    $('.targetDiv').hide();
    $('#div' + $(this).attr('target')).show();
});

$('.targetDivs').hide();
$('.show2').click(function () {
    $('.targetDivs').hide();
    $('#div' + $(this).attr('target')).show();
});

$('.targetDivss').hide();
$('.show3').click(function () {
    $('.targetDivss').hide();
    $('#div' + $(this).attr('target')).show();
});
</script>


<!-- Today cript -->


<!-- For Macbook Air (2009 - Late 2010) -->
<script>
$(document).ready(function()  {
  $(".air_type_1").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_air_1').val(t2);
  },
  
    
});
  });
}); 
</script>

<script>
$(document).ready(function()  {
  $(".air_type_2").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_air_2').val(t2);
  },
  
    
});
  });
}); 
</script>

<script>
$(document).ready(function()  {
  $(".air_type_3").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_air_3').val(t2);
  },
  
    
});
  });
}); 
</script>

<script>
$(document).ready(function()  {
  $(".air_type_4").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_air_4').val(t2);
  },
  
    
});
  });
}); 
</script>

<!-- End For Type -->

<script>
$(document).ready(function()  {
  $(".air_prs_1_1").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_air_1_core2').val(t2);
  },
 
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_air_11_1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_air_11_1_11').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_air_11_2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_air_11_1_13').val(t2);
  },
  
    
});
  });
}); 

</script>
<!-- End Macbook Air (2009 - Late 2010) -->


<!-- For Processor -->

<script>
$(document).ready(function()  {
  $(".air_prs_i3").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_air_prs_i3').val(t2);
  },
 
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".air_prs_i5").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_air_prs_i5').val(t2);
  },
 
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".air_prs_i7").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_air_prs_i7').val(t2);
  },
 
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".air_prs_m1").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_air_prs_m1').val(t2);
  },
 
});
  });
}); 

</script>
<!-- End For Processor -->


<!-- For Screen -->

<script>
$(document).ready(function()  {
  $(".s_air_11").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_air_11').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_air_13").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_air_13').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_air_15").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_air_15').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_air_16").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_air_16').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_air_17").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_air_17').val(t2);
  },
  
    
});
  });
}); 

</script>

<!-- End screen -->