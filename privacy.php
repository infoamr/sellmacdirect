<?php include 'layouts/header.php';?>
<section class="ftco-section" style="background-image: linear-gradient(to right bottom, #9dc88d, #a8c77e, #b6c46e, #c6c060, #d8bb54, #c6b24c, #b5a945, #a4a03f, #728e42, #487947, #286247, #164a41);">
      <div class="overlay"></div>
      <div class="container mt-5">
        <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <span class="subheading">Privacy Policy</span>
            <h2 class="mb-2">The smartest way to sell your used mac</h2>
          </div>
        </div>
        <div class="row d-flex">
          <div class="col-md-12 col-lg-12">
          	    <h5 style="text-align: justify;">In this privacy policy references to "we", "us" and "our" are to Sellmacdirect.co.uk. References to "our Website" or "the Website" are to www.sellmacdirect.co.uk</h5>
          	    <p style="text-align: justify;"><b>The information we collect via the Website may include.</b><br>
          	    	<ol>
          	    		<li>Any personal details you knowingly provide us with through forms and our email, such as name, address, telephone number etc.</li>
          	    		<li>In order to effectively process credit or debit card transactions it may be necessary for the bank or card processing agency to verify your personal details for authorisation outside the European Economic Area (EEA). Such information will not be transferred out of the EEA for any other purpose.</li>
          	    	</ol>
          	    	<b>The cookies used on our site are as follows:</b><br>
          	    	<span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
          	    	<b>"Session" Cookies</b>
          	    	<p>We use a session cookie to remember your log-in for you and what you’ve put in the basket. These we deem strictly necessary to the working of the website. If these are disabled then various functionality on the site will be broken. More information on session cookies and what they are used for at<br>
          	    	<a href="http://www.allaboutcookies.org/cookies/session-cookies-used-for.html">http://www.allaboutcookies.org/cookies/session-cookies-used-for.html</a></p>

          	    	<span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
          	    	<b>Google Analytics</b>
          	    	<p>we use this to understand how the site is being used in order to improve the user experience. User data is all anonymous. You can find out more about Google’s position on privacy as regards its analytics service at</br>
          	    	<a href="http://www.google.co.uk/intl/en/analytics/privacyoverview.html">http://www.google.co.uk/intl/en/analytics/privacyoverview.html</a></p>

          	    	<span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
          	    	<b>Twitter</b>
          	    	<p>we use this to enable sharing of content on the social network Twitter. You can find out more about Twitters's position on privacy as regards its service at</br>
          	    	<a href="https://twitter.com/privacy/">https://twitter.com/privacy/</a></p>

                  <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
                  <b>Facebook</b>
                  <p>we use this to enable sharing of content on the social network Facebook. You can find out more about Facebooks's position on privacy as regards its service at</br>
                  <a href="http://www.facebook.com/about/privacy/">http://www.facebook.com/about/privacy/</a></p>

                  <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
                  <b>Google+</b>
                  <p>we use this to enable sharing of content on the social network Google+. You can find out more about Google's position on privacy as regards its service at</br>
                  <a href="http://www.google.com/policies/privacy/">http://www.google.com/policies/privacy/</a><br><br>

                  <b>What we do with your information</b><br>
                  Any personal information we collect from this website will be used in accordance with the Data Protection Act 1998 and other applicable laws. The details we collect will be used<br><br>

                  <b>To process your order, to provide after sales service.</b><br>
                  Any other websites which may be linked to by our website are subject to their own policy, which may differ from ours.

                </p>





          	    	<!-- <ul>
          	    		<li style="list-style: none;"><b>"Session" Cookies</b></li>
          	    		<p>We use a session cookie to remember your log-in for you and what you’ve put in the basket. These we deem strictly necessary to the working of the website. If these are disabled then various functionality on the site will be broken. More information on session cookies and what they are used for at<br>
          	    			<a href="http://www.allaboutcookies.org/cookies/session-cookies-used-for.html">http://www.allaboutcookies.org/cookies/session-cookies-used-for.html</a>
          	    			<br>Persistent Cookies for Site Analytics and Performance	</p>

          	    		<li>Google Analytics</li>
          	    		<p>we use this to understand how the site is being used in order to improve the user experience. User data is all anonymous. You can find out more about Google’s position on privacy as regards its analytics service at <br>
          	    			<a href="http://www.google.co.uk/intl/en/analytics/privacyoverview.html">http://www.google.co.uk/intl/en/analytics/privacyoverview.html</a>

          	    		<li>Twitter</li>
          	    		<p>we use this to enable sharing of content on the social network Twitter. You can find out more about Twitters's position on privacy as regards its service at<br>
          	    			<a href="https://twitter.com/privacy/">https://twitter.com/privacy/</a>

          	    		<li>Facebook</li>
          	    		<p>we use this to enable sharing of content on the social network Facebook. You can find out more about Facebooks's position on privacy as regards its service at<br>
          	    			<a href="http://www.facebook.com/about/privacy/">http://www.facebook.com/about/privacy/</a>

          	    		<li>Google+</li>
          	    		<p>we use this to enable sharing of content on the social network Google+. You can find out more about Google's position on privacy as regards its service at<br>
          	    			<a href="http://www.google.com/policies/privacy/">http://www.google.com/policies/privacy/</a>	
          	    	</ul>

          	    	<b>What we do with your information</b><br>
          	    	Any personal information we collect from this website will be used in accordance with the Data Protection Act 1998 and other applicable laws. The details we collect will be used<br>

          	    	<b>To process your order, to provide after sales service.</b><br>
          	    	<p>Any other websites which may be linked to by our website are subject to their own policy, which may differ from ours.</p>
          	    </p> -->
          </div>


          
        </div>
      </div>
    </section>














<?php include 'layouts/footer.php';?>