<?php include 'layouts/header.php';?>
    <div class="hero-wrap ftco-degree-bg" style="background-image: linear-gradient(to left, #4d774e, #63854c, #7d9248, #9c9e44, #bea843, #c7ae49, #cfb54e, #d8bb54, #c6c060, #b6c46e, #a8c77e, #9dc88d);" data-stellar-background-ratio="0.5">
      <!--<div class="overlay"></div>-->
      <div class="container">
        <div class="row no-gutters slider-text justify-content-center align-items-center">
          <div class="col-lg-8 col-md-6 ftco-animate d-flex align-items-end">
          	<div class="text text-center">
          	    <br><br>
	            <h1 class="mb-4">Get The Best Offer For Your Used MAC</h1>
	            <p style="font-size: 18px;">Find Out The Serial Number Of Your MAC</p>
	            <form action="lookup.php" method="get" class="search-location mt-md-5">
		        		<div class="row justify-content-center">
		        			<div class="col-lg-10 align-items-end">
		        				<div class="form-group">
		          				<div class="form-field">
				                <input type="text" name="serial_no" class="form-control" placeholder="Enter the serial number here to get an offer (e.g.C02P42MEG3QD)">
				                <button type="submit" style="background-color:#1c3730 !important" class="icon mb-3 d-flex align-items-center justify-content-center"><span class="ion-ios-search" style="padding-left:0px!important"></span></button>
				              </div>
			              </div>
		        			</div>
		        		</div>
		        </form>
            </div>
          </div>
        </div>
      </div>
      <div class="mouse">
				<a href="#" class="mouse-icon">
					<div class="mouse-wheel"><span class="ion-ios-arrow-round-down"></span></div>
				</a>
			</div>
    </div>

    <section class="ftco-section ftco-degree-bg" style="background-image: linear-gradient(to right bottom, #9dc88d, #a8c77e, #b6c46e, #c6c060, #d8bb54, #c6b24c, #b5a945, #a4a03f, #728e42, #487947, #286247, #164a41);">
    	<div class="overlay"></div>
    	<div class="container">
    		<div class="row justify-content-start mb-5">
          <div class="col-md-12 text-center heading-section heading-section-white ftco-animate">
          	<span class="subheading">Work flow</span>
            <h2 class="mb-3">How it works</h2>
          </div>
        </div>
    		<div class="row">
    			<div class="work-flow col-md-12 col-lg-12">
    				<div class="row">
		    			<div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
		            <div class="media block-6 services services-2">
		              <div class="media-body py-md-4 text-center">
		              	<div class="icon mb-3 d-flex align-items-center justify-content-center"><span>01</span></div>
		                <h3>Select gadget & Get best value</h3>
		                <p style="font-size:18px">Get instant and fair online price for your gadget.</p>
		              </div>
		            </div>      
		          </div>
		          <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
		            <div class="media block-6 services services-2">
		              <div class="media-body py-md-4 text-center">
		              	<div class="icon mb-3 d-flex align-items-center justify-content-center"><span>02</span></div>
		                <h3>Get Free Doorstep Pickup</h3>
		                <p style="font-size:18px">Scheduled free pick up service within a week as per your convenience.</p>
		              </div>
		            </div>      
		          </div>
		          <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
		            <div class="media block-6 services services-2">
		              <div class="media-body py-md-4 text-center">
		              	<div class="icon mb-3 d-flex align-items-center justify-content-center"><span>03</span></div>
		                <h3>Fully removed your Gadget data</h3>
		                <p style="font-size:18px">All data on the gadget is fully destroyed.</p>
		              </div>
		            </div>      
		          </div>
		          <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
		            <div class="media block-6 services services-2">
		              <div class="media-body py-md-4 text-center">
		              	<div class="icon mb-3 d-flex align-items-center justify-content-center"><span>04</span></div>
		                <h3>Get the Payment Instantly</h3>
		                <p style="font-size:18px">Our professional inspects the product and sends you the payment instantly.</p>
		              </div>
		            </div>      
		          </div>
		        </div>
		      </div>
    		</div>
    	</div>
    </section>

    <section class="ftco-section ftco-degree-bg" style="background-image: linear-gradient(to left bottom, #164a41, #286247, #487947, #728e42, #a4a03f, #ada340, #b5a541, #bea843, #9c9e44, #7d9248, #63854c, #4d774e);">
      <div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-start mb-5">
          <div class="col-md-12 text-center heading-section heading-section-white ftco-animate">
            <h2 class="mb-3">Couldn't find the serial for your device?</h2>
          </div>
        </div>
        <div class="row">
          <div class="h-device col-md-12 col-lg-12">
            <div class="row">
              <div class="col-md-12 col-lg-4 d-flex align-self-stretch ftco-animate">
                  <div class="media-body py-md-4 text-center">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                      <a href="macbook.php">
                        <!--<div class='box-image'>-->
                          <img src="images/macbook.svg" alt="mac" height="80px;" width="80px;">
                        <!--</div>-->
                      </a>
                    </div>
                    <h3>MACBOOK</h3>
                  </div>
              </div>
              <div class="col-md-12 col-lg-4 d-flex align-self-stretch ftco-animate">
                  <div class="media-body py-md-4 text-center">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                      <a href="macbookpro.php">
                        <!--<div class='box-image'>-->
                          <img src="images/macbook.svg" alt="mac" height="80px;" width="80px;">
                        <!--</div>-->
                      </a>
                    </div>
                    <h3>MACBOOK PRO</h3>
                  </div>
              </div>
              <div class="col-md-12 col-lg-4 d-flex align-self-stretch ftco-animate">
                  <div class="media-body py-md-4 text-center">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                      <a href="macbookair.php">
                        <!--<div class='box-image'>-->
                          <img src="images/macbook.svg" alt="mac" height="80px;" width="80px;">
                        <!--</div>-->
                      </a>
                    </div>
                    <h3>MACBOOK AIR</h3>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-no-pb">
      <div class="container">
        <div class="row no-gutters">
          <div class="col-md-6 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url(images/sellupgrade.jpg);">
          </div>
          <div class="col-md-6 wrap-about py-md-5 ftco-animate">
            <div class="heading-section p-md-5">
              <h2 class="mb-4">Sell or Upgrade</h2>

              <p style="color: #000; text-align: justify;">You landed in the right place. At Sell Mac Direct, you can sell your Apple devices or mac products with ease. We will purchase your used Mac products through a hassle-free and quick transaction.</p>
              <p style="color: #000; text-align: justify;">Our aim is to help individuals, businesses & students upgrade their Apple laptops, Macbook Pro, iOS devices, mac products, or desktop computers by simply offering to pay lucrative prices for their used gadgets which include, Macbook Airs, Apple TVs, MacBooks, iMac Pros, Mac Pros, iPhones, Mac Minis, iPads, iPods & iMacs.</p>
              <p style="color: #000; text-align: justify;">Served & trusted by 5000+ customers in the last 5 years, and we feel blessed to have delivered the highest level of customer service possible and more to go. We also have the facilities of phone support and free live chat for all purchases. We deliver 100% customer satisfaction and follow up with each and every customer after the sale has been made.</p>
              <p style="color: #000; text-align: justify;">Cost-free and secure shipping is included for all purchases and provides the best reselling price for your gadgets. We provide 24*7 help and support.</p>
              <p style="color: #000; text-align: justify;">Dial <strong>02085 7598 50</strong> or feel free to reach us at <strong>sales@sellmacdirect.co.uk</strong></p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-no-pb">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <span class="subheading">Our Services</span>
            <h2 class="mb-2">The smartest way to sell your used mac</h2>
          </div>
        </div>
        <div class="row d-flex">
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center"><span class=""><img src="images/simple_safe.png"></span></div>
            <div class="media-body py-md-4">
            <h3>Simple & Safe</h3>
            <p style="font-size:18px">Instant online offers for MacBook, MacBook Pro and Macbook Air with a minimal amount of information.</p>
            </div>
            </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center"><span><img src="images/fast.png"></span></div>
            <div class="media-body py-md-4">
            <h3>Fastest</h3>
            <p style="font-size:18px">Free secure packaging and express pickup are provided 7 days a week with an interval of 4 hour.</p>
            </div>
            </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center"><span class="flaticon-locked"></span></div>
            <div class="media-body py-md-4">
            <h3>Secure</h3>
            <p style="font-size:18px">All data on the device will be permanently deleted, and a certificate is provided to give you peace of mind.</p>
            </div>
            </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center"><span class="flaticon-file"></span></div>
            <div class="media-body py-md-4">
            <h3>Trusted</h3>
            <p style="font-size:18px">We pay you fast! Payment will be made to your account (Bank or PayPal) within two days after receiving your item.</p>
            </div>
        </div>
        </div>
        </div>
      </div>
    </section>

		<section class="ftco-counter img" id="section-counter">
    	<div class="container">
    		<div class="row">
          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
            <div class="block-18 py-4 mb-4">
              <div class="text text-border d-flex align-items-center">
                
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
            <div class="block-18 py-4 mb-4">
              <div class="text text-border d-flex align-items-center">
                
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
            <div class="block-18 py-4 mb-4">
              <div class="text text-border d-flex align-items-center">
                
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
            <div class="block-18 py-4 mb-4">
              <div class="text d-flex align-items-center">
                
              </div>
            </div>
          </div>
        </div>
    	</div>
    </section>		

<?php include 'layouts/footer.php';?>